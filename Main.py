# Import statements
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer

#%% Load data and define features

data_train = pd.read_csv('train.csv')

features = ["Age", "Fare","Sex","Pclass","SibSp", "Parch"]
  
#%% Create fillNansImputer transformer for Pipeline

def fillNans(pdfeatures):
    imp = SimpleImputer(strategy='median')
    return imp.fit_transform(pdfeatures)
    
from sklearn.preprocessing import FunctionTransformer
transformer = FunctionTransformer(fillNans, validate=False)

# Create OneHotEncoder for Sex
    
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
ct = ColumnTransformer([('one_hot_encoder', OneHotEncoder(
    categories=[('male','female')]),['Sex'])], remainder='passthrough')
    
hs_train_transformed = ct.fit_transform(data_train[features])
data_train['Sex'] = hs_train_transformed[:,0]

#%% Create a pipeline and test it with crossvalidation

from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV

pipe = Pipeline([('fillNaNs',transformer), ('scaler', StandardScaler()), ('svc', SVC(probability=True))])

scores = cross_val_score(pipe, data_train[['Age','Sex']],data_train[['Survived']], cv=5)

#%% Grid search an SVC algorithm

param_grid = {
    'svc__C' : np.logspace(-4, 3, 10),
    'svc__kernel' : ['linear','poly','rbf','sigmoid'],
    'svc__gamma' : ['scale','auto']
    }

pipe = Pipeline([('fillNaNs',transformer), ('scaler', StandardScaler()), ('svc', SVC())])
search = GridSearchCV(pipe, param_grid, n_jobs=-1)
search.fit(data_train[features], data_train['Survived'])
print("Best parameter (CV score=%0.3f):" % search.best_score_)
print(search.best_params_)
print(search.best_score_)

 
#%% Create prediction on test data 

x_test = pd.read_csv('test.csv')
hs_train_transformed = ct.fit_transform(x_test[features])
x_test['Sex'] = hs_train_transformed[:,0]
   
pred = search.predict(x_test[features])

csvdata2 = pd.DataFrame({'PassengerId' : x_test.PassengerId, 'Survived': pred})
csvdata2.to_csv('my_submission6.csv', index=False)
